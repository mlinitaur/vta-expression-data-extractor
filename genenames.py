import os

file = open("mouse_expression_data_sets.csv", "r")
output = open("output.csv", "r").readlines()

for line in output:
    sep = line.split(",")

    gene_id = sep[1]

    for l in file:
        lines = l.split(",")

        id = lines[1]

        if id == gene_id:
            gene_symbol = lines[2]
