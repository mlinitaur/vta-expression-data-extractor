import os

existing = open("749_output.csv", "r").readlines()
output = open("final_output.csv", "w")
file_list = ["374", "381", "100", "214", "331"]


s1 = "N/A"
s2 = "N/A"
s3 = "N/A"
s4 = "N/A"
s5 = "N/A"
for line in existing:
    line = line.rstrip("\n")
    line_separated = line.split(",")
    current_experiment_id = line_separated[4]
    print(current_experiment_id)

    if line.endswith(("N/A","N/A,")):
        print("N/A")
        continue

    for file in file_list:
        current_file = open("{}.csv".format(file), "r").readlines()
        print(file)

        for working_line in current_file:
            working_line = working_line.rstrip("\n")
            working_line_separated = working_line.split(",")
            working_experiment_id = working_line_separated[2]

            if current_experiment_id == working_experiment_id:
                print("Success!")
                if file == "374":
                    s1 = working_line
                elif file == "381":
                    s2 = working_line
                elif file == "100":
                    s3 = working_line
                elif file == "214":
                    s4 = working_line
                elif file == "331":
                    s5 = working_line

            else:
                continue
    print("\n")
    try:
        output.write("{},{},{},{},{},{}\n".format(line,s1,s2,s3,s4,s5))
    except NameError:

        output.write("{},{},{},{},{},{}\n".format(line,s1,s2,s3,s4,s5))
