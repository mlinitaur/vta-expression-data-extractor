import os

local = open("output.csv", "r").readlines()
test = open("test.csv", "r").readlines()
output = open("final_output.csv", "w")
printed = False

for line in test:
    line = line.rstrip("\n")
    commas = line.split(",")
    gene_symbol = commas[0]
    print("\n")
    print(gene_symbol)

    for gene in local:
        split = gene.split(",")
        local_gene_symbol = split[2]
        print(local_gene_symbol)

        if gene_symbol == local_gene_symbol:
            printed = True
            output.write("{},{}".format(line, gene))
            print("Success")

        else:
            continue
    if printed == False:

        output.write("{},N/A,".format(line))

    else:
        continue
